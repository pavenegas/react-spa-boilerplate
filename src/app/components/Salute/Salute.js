import React, { useState } from 'react';
import Button from '../../components/Button';
import request from '../../../utils/request';

import './styles.scss';

// const name = 'peter'

const Salute = () => {
  const [name, setName] = useState('Peter');
  const [pokemons, setPokemons] = useState([]);
  const [loading, setLoading] = useState(false);

  const handleClick = () => {
    setName('Juan');
  };

  const getPokemons = () => {
    setLoading(true);

    try {
      setTimeout(async () => {
        const response = await request.get('https://pokeapi.co/api/v2/pokemon?limit=20');
        setPokemons(response.data.results);
        setLoading(false);
      }, 1500);
    } catch (err) {
      throw err;
    }
  };

  return (
    <div
      className="salute__wrapper"
      id="name">
      <h1> {name} </h1>

      <button onClick={handleClick}> Change name </button>

      <Button
        label="im the button"
        salute={getPokemons}
      />


      {loading ? (
        <p> Im loading ...</p>
      ) : (
        <ul>
          {pokemons.map((pokemon) => {
            return <li key={pokemon.name}> {pokemon.name} </li>;
          })}
        </ul>
      )}
    </div>
  );
};

export default Salute;
