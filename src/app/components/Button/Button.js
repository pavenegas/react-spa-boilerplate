import React from 'react';
import PropTypes from 'prop-types';

const Button = (props) => {
  const { salute, label } = props;

  return (
    <button onClick={salute}> {label} </button>
  );
};

Button.propTypes = {
  label: PropTypes.string,
  salute: PropTypes.func,
};

export default Button;
