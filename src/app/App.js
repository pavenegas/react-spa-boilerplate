import React from 'react';
import { hot } from 'react-hot-loader/root';

import Salute from './components/Salute';

const App = () => {
  return (
    <div
      className="app"
      id="app"
    >
      <Salute />
    </div>
  );
};

export default hot(App);
