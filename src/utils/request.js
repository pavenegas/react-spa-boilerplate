import axios from 'axios';

export default {
  get: (...args) => axios.get(...args),
  post: (...args) => axios.post(...args),
};
