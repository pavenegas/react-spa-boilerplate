import React from 'react';
import reactDOM from 'react-dom';
import App from './app/App';

const root = document.querySelector('#root');

reactDOM.render(<App />, root);
