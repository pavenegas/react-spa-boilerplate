const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',

  entry: ['react-hot-loader/patch', './src/index.js'],
  output: {
    path: path.join(__dirname, '../dist'),
    filename: 'build-[hash].js',
  },

  module: {
    rules: [
      {
        test: /\.js$/i,
        use: 'babel-loader',
      },
      {
        test: /\.s[ac]ss$/i,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
    ],
  },

  devServer: {
    hot: true,
    host: '0.0.0.0',
  },

  resolve: {
    alias: {
      'react-dom': '@hot-loader/react-dom',
    },
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: 'React Boilerplate',
      filename: 'index.html',
      template: path.join(__dirname, '../', '/src/statics/index.html'),
    }),
  ],
};
