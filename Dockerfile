FROM node:12-alpine

WORKDIR /app

COPY ./package.lock ./yarn.lock ./

RUN yarn install